#pragma once

#if __has_include( <SRSignal/SRSignal.hpp>)
#	include <SRSignal/SRSignal.hpp>
#endif

class SRCursor {
	bool _localState;

public:
	SRCursor() noexcept;
	~SRCursor() noexcept;

	void showCursor() noexcept;
	void hideCursor() noexcept;

	void toggleCursor( bool state ) noexcept;

	static bool isCursorShowed() noexcept;
	static bool isCursorHidden() noexcept;

	[[nodiscard]] bool isLocalCursorShowed() const noexcept;
	[[nodiscard]] bool isLocalCursorHidden() const noexcept;

	[[nodiscard]] bool isShowed() const noexcept;
	[[nodiscard]] bool isHidden() const noexcept;

	void update() const noexcept;

#if __has_include( <SRSignal/SRSignal.hpp>)
	SRSignal<> onShow;
	SRSignal<> onHide;
	SRSignal<bool> onToggle;
	mutable SRSignal<bool> onUpdate;
#endif
};
