#include "SRCursor.h"
#include <cstdint>
#if __has_include( <windows.h>)
#	include <windows.h>
#else
static_assert( false, "Can't include <windows.h>. This library support only win32 projects!" );
#endif
#if __has_include( <llmo/memsafe.h>)
#	include <llmo/memsafe.h>
#else
static_assert( false,
			   "Can't include <llmo/memsafe.h>. You project contain library llmo?\n"
			   "Solution of this problem:\n"
			   "1. Download library https://gitlab.com/SR_team/llmo\n"
			   "2. Build library. For CMake add \"add_subdirectory(llmo)\"\n"
			   "3. Link library with project. For CMake add \"target_link_libraries(${PROJECT_NAME} llmo)\"\n"
			   "4. Add library to global include. For CMake add \"target_include_directories(${PROJECT_NAME} PUBLIC llmo)\"\n" );
#endif
#if __has_include( <llmo/callfunc.hpp>)
#	include <llmo/callfunc.hpp>
#endif

SRCursor::SRCursor() noexcept {
	_localState = false;
}

SRCursor::~SRCursor() noexcept {
	hideCursor();
}

void SRCursor::showCursor() noexcept {
	// disable keyboard process
#if __has_include( <llmo/memsafe.h>)
	memsafe::set( (void *)0x541DF5, 0x90, 5 );
	// disable mouse process
	memsafe::set( (void *)0x53F417, 0x90, 5 );
	// disable camera process
	memsafe::copy( (void *)0x53F41F, { 0x33, 0xC0, 0x0F, 0x84 } );
	memsafe::write<uint8_t>( (void *)0x6194A0, 0xC3 );
#endif
	// show cursor
	_localState = true;
#if __has_include( <windows.h>)
	SetCursor( GetCursor() != nullptr ? GetCursor() : LoadCursor( nullptr, IDC_ARROW ) );
#endif
#if __has_include( <SRSignal/SRSignal.hpp>)
	onShow();
	onToggle( true );
#endif
}

void SRCursor::hideCursor() noexcept {
#if __has_include( <llmo/memsafe.h>)
	memsafe::copy( (void *)0x541DF5, { 0xE8, 0x46, 0xF3, 0xFE, 0xFF } );
	memsafe::copy( (void *)0x53F417, { 0xE8, 0xB4, 0x7A, 0x20, 0x00 } );
	memsafe::copy( (void *)0x53F41F, { 0x85, 0xC0, 0x0F, 0x8C } );
	memsafe::write<uint8_t>( (void *)0x6194A0, 0xE9 );
	// clear mouse event
	memsafe::set( (void *)0xB73424, 0x0, 8 ); // clear pos
#endif
#if __has_include( <llmo/callfunc.hpp>)
	CallFunc::ccall( 0x541BD0 ); // CPad::ClearMouseHistory()
	CallFunc::ccall( 0x541DD0 ); // CPad::UpdatePads()
#endif
	_localState = false;
#if __has_include( <windows.h>)
	SetCursor( LoadCursor( nullptr, nullptr ) );
#endif
#if __has_include( <SRSignal/SRSignal.hpp>)
	onHide();
	onToggle( false );
#endif
}

void SRCursor::toggleCursor( bool state ) noexcept {
	if ( state )
		showCursor();
	else
		hideCursor();
}

bool SRCursor::isCursorShowed() noexcept {
	return !isCursorHidden();
}

bool SRCursor::isCursorHidden() noexcept {
#if __has_include( <llmo/memsafe.h>)
	if ( memsafe::compare( (void *)0x541DF5, { 0xE8, 0x46, 0xF3, 0xFE, 0xFF } ) &&
		 memsafe::compare( (void *)0x53F417, { 0xE8, 0xB4, 0x7A, 0x20, 0x00 } ) &&
		 memsafe::compare( (void *)0x53F41F, { 0x85, 0xC0, 0x0F, 0x8C } ) && memsafe::read<uint8_t>( (void *)0x6194A0 ) == 0xE9 &&
		 GetCursor() == nullptr )
		return true;
#endif
	return false;
}

bool SRCursor::isLocalCursorShowed() const noexcept {
	return _localState;
}

bool SRCursor::isLocalCursorHidden() const noexcept {
	return !_localState;
}

bool SRCursor::isShowed() const noexcept {
	return _localState && isCursorShowed();
}

bool SRCursor::isHidden() const noexcept {
	return !isShowed();
}

void SRCursor::update() const noexcept {
	if ( isLocalCursorShowed() ) {
		if ( isCursorHidden() ) {
#if __has_include( <llmo/memsafe.h>)
			// disable keyboard process
			memsafe::set( (void *)0x541DF5, 0x90, 5 );
			// disable mouse process
			memsafe::set( (void *)0x53F417, 0x90, 5 );
			// disable camera process
			memsafe::copy( (void *)0x53F41F, { 0x33, 0xC0, 0x0F, 0x84 } );
			memsafe::write<uint8_t>( (void *)0x6194A0, 0xC3 );
#endif
#if __has_include( <windows.h>)
			SetCursor( GetCursor() != nullptr ? GetCursor() : LoadCursor( nullptr, IDC_ARROW ) );
#endif
#if __has_include( <SRSignal/SRSignal.hpp>)
			onUpdate( true );
#endif
		}
#if __has_include( <llmo/memsafe.h>)
		// clear mouse event
		memsafe::set( (void *)0xB73424, 0x0, 8 ); // clear pos
#endif
#if __has_include( <llmo/callfunc.hpp>)
		CallFunc::ccall( 0x541BD0 ); // CPad::ClearMouseHistory()
		CallFunc::ccall( 0x541DD0 ); // CPad::UpdatePads()
#endif
	} else if ( isCursorShowed() ) {
#if __has_include( <llmo/memsafe.h>)
		memsafe::copy( (void *)0x541DF5, { 0xE8, 0x46, 0xF3, 0xFE, 0xFF } );
		memsafe::copy( (void *)0x53F417, { 0xE8, 0xB4, 0x7A, 0x20, 0x00 } );
		memsafe::copy( (void *)0x53F41F, { 0x85, 0xC0, 0x0F, 0x8C } );
		memsafe::write<uint8_t>( (void *)0x6194A0, 0xE9 );
		// clear mouse event
		memsafe::set( (void *)0xB73424, 0x0, 8 ); // clear pos
#endif
#if __has_include( <llmo/callfunc.hpp>)
		CallFunc::ccall( 0x541BD0 ); // CPad::ClearMouseHistory()
		CallFunc::ccall( 0x541DD0 ); // CPad::UpdatePads()
#endif
#if __has_include( <windows.h>)
		SetCursor( LoadCursor( nullptr, nullptr ) );
#endif
#if __has_include( <SRSignal/SRSignal.hpp>)
		onUpdate( false );
#endif
	}
}
