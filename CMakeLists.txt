cmake_minimum_required(VERSION 3.0)
project(SRCursor CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED YES)
set(CMAKE_CXX_EXTENSIONS NO)

add_library(SRCursor STATIC SRCursor.cpp SRCursor.h)
target_link_libraries(SRCursor llmo)
